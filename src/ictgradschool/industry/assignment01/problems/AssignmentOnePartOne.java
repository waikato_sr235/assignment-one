package ictgradschool.industry.assignment01.problems;

/**
 * Please run TestAssignmentOnePartOne to check your answers.
 * There are 10 exercises in this class. They are ordered roughly in increasing order of difficulty.
 * You can do them in any order you like. Each exercise is worth the same amount towards your overall Assignment 1 mark.
 * However, you will only get full marks for each exercise on confirmation by the markers that you have in fact
 * correctly completed the exercise. It is not enough that you have passed all of the tests.
 *
 * You may modify the code in between the comments: // Answer here // . Do not modify other parts of the code.
 *
 * Write your name and ID here: Sam Rosenberg and 1553437.
 */
public class AssignmentOnePartOne {

    /**
     * Q1. Complete the method divideTwoInts, that will, when given two integer parameters,
     * return the quotient, which is also an integer.
     */
    public int divideTwoInts(int dividend, int divisor) {
        int quotient = 0;
        // Answer here
        quotient = dividend / divisor;
        //
        return quotient;
    }

    /**
     * Q2. Complete the method called multiplyTwoDoubles that will, when given two double parameters,
     * return their product as an integer.
     */
    public int multiplyTwoDoubles(double multiplicand, double multiplier) {
        int product = 0;
        // Answer here
        product = (int)(multiplicand * multiplier);
        //
        return product;
    }

    /**
     * Q3. Complete the method maxOfTwoNumbers that takes two integer parameters, and returns the larger of the two
     * (or either of them if they are the same). Do not use if statements. Hint: Use Math.max()
     */
    public int maxOfTwoNumbers(int numberOne, int numberTwo) {
        // Answer here
        return Math.max(numberOne, numberTwo);
        //
    }

    /**
     * Q4. Complete the method convertCharToString that takes a character parameter and returns a String consisting of
     * just that character. Hint: Understand how string concatenation works in Java.
     */
    public String convertCharToString(char character) {
        // Answer here
        return character + "";
        //
    }

    /**
     * Q5. Complete the method getFirstThreeLetters that takes a String parameter, and returns the first three letters of the String.
     * You may assume that the String is always non-empty and always has more than three letters.
     */
    public String getFirstThreeLetters(String text) {
        // Answer here
        return text.substring(0, 3);
        //
    }

    /**
     * Q6. Complete the method legalToBuyDrinks that takes an integer parameter representing a person's age
     * and returns a boolean value: true if the person is old enough to buy alcohol in New Zealand (minimum age 18)
     * otherwise false.
     */
    public boolean legalToBuyDrinks(int age) {
        // Answer here
        int drinkingAge = 18;
        return age >= drinkingAge;
        //
    }

    /**
     * Q7. Complete the method eligibleToVote that takes a boolean parameter representing if a person is a
     * New Zealand resident, and an integer parameter representing a person's age. The method returns a boolean value:
     * true if the person is a New Zealand resident and is between the age of 18 and 150 inclusively, otherwise false.
     */
    public boolean eligibleToVote(boolean nzResident, int age) {
        // Answer here
        int minVotingAge = 18;
        int maxVotingAge = 150;
        boolean isLegalAge = age >= minVotingAge && age <= maxVotingAge;
        return isLegalAge && nzResident;
        //
    }

    /**
     * Q8. Complete the method called implies that takes two boolean parameters a and b,
     * and returns the result of the boolean expression a => b. That is, if a is true and b is true, the result is true.
     * If a is false, the result is true. Otherwise, the result is false.
     */
    public boolean implies(boolean a, boolean b) {
        //Answer here;
        return !a || b;
        //
    }

    /**
     * Q9. Complete the method isSubstring that takes two String parameters representing two strings,
     * and returns a String value:
     * "Same string" if the two strings match exactly, or
     * "First string is a substring of second string", or
     * "Second string is a substring of first string, otherwise "No match".
     *
     * Note that this method is case sensitive and ignores spaces.
     */
    public String isSubstring(String firstStr, String secondStr) {
        //Answer here
        String exactMatch = "Same string";
        String substringOfSecond = "First string is a substring of second string";
        String substringOfFirst = "Second string is a substring of first string";
        String noMatch = "No match";

        String firstWithoutSpaces = firstStr.replaceAll("\\s","");
        String secondWithoutSpaces = secondStr.replaceAll("\\s","");

        int indexOfSecondInFirst = firstWithoutSpaces.indexOf(secondWithoutSpaces);
        int indexOfFirstInSecond = secondWithoutSpaces.indexOf(firstWithoutSpaces);

        boolean isSubstringOfFirst = indexOfSecondInFirst > indexOfFirstInSecond;
        boolean isNoMatch = indexOfSecondInFirst + indexOfFirstInSecond == -2; //substring not found returns -1; -2 means neither matched

        if (firstWithoutSpaces.equals(secondWithoutSpaces)) {
            return exactMatch;
        } else if (isNoMatch) {
            return noMatch;
        } else if (isSubstringOfFirst) {
            return substringOfFirst;
        } else {
            return substringOfSecond;
        }
        //
    }

    /**
     * Q10. Complete the method medianOfThreeInts that takes three integer parameters, and
     * returns the median of the three values. That is, the method returns the second largest (or
     * second smallest) of the three values.
     */
    public int medianOfThreeInts(int numOne, int numTwo, int numThree) {
        // Answer here
        return Math.max(Math.min(numOne, numTwo), Math.max(Math.min(numTwo, numThree), Math.min(numOne, numThree)));
        //
    }
}