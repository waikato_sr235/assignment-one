package ictgradschool.industry.assignment01.problems;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Please run TestAssignmentOnePartThree to check your answers.
 * There are 10 exercises in this class. You can do them in any order you like.
 * Each exercise is worth the same amount towards your overall Assignment 1 mark.
 * However, you will only get full marks for each exercise on confirmation by the markers that you have in fact
 * correctly completed the exercise. It is not enough that you have passed all of the tests.
 *
 * You may modify the code in between the comments: // Answer here // . Do not modify other parts of the code.
 *
 * Write your name and ID here: name and ID.
 */
public class AssignmentOnePartThree {
    /**
     * Q1. Complete the method findOccurrenceOfACharacter that returns the number of occurrences of a given character
     * (case insensitive) in a String.
     * For example, findOccurrenceofACharacter("Hello World", 'o') will return 2, and
     * findOccurrenceofACharacter("Goodbye Earth", 'z') will return 0.
     */
    public int findOccurrenceOfACharacter(String searchString, char targetChar) {
        //Answer here
        String searchKey = "" + targetChar;
        String adjustedString = searchString.toLowerCase().replaceAll(searchKey.toLowerCase(),"");
        return searchString.length() - adjustedString.length();
        //
    }

    /**
     * Q2. Complete the method zip that takes two non-negative integer numbers, and returns the decimal zip (which is
     * also an integer). The decimal zip, C, of two non-negative integers A and B is created as follows:
     *
     * 1. the first digit of C is the first digit of A;
     * 2. the second digit of C is the first digit of B;
     * 3. the third digit of C is the second digit of A;
     * 4. the fourth digit of C is the second digit of B;
     * 5. and so forth
     *
     * If one of the integers A and B runs out of digits, the remaining digits of the other integer are appended to C.
     *
     * For example, zip(12, 56) will return 1526
     *                    zip(56, 12) will return 5162
     *                    zip(12345, 678) will return 16273845
     *                    zip(123, 67890) will return 16273890
     *
     * The method returns 0 if one of the given integer is also 0.
     */
    public int zip(int a, int b) {
        //Answer here
        String aAsString = a + "";
        String bAsString = b + "";

        String resultAsString = "";
        for (int i = 0; i < Math.min(aAsString.length(), bAsString.length()); i++) {
            resultAsString += aAsString.charAt(i) + "" + bAsString.charAt(i);
        }

        if (aAsString.length() > bAsString.length()) {
            resultAsString += aAsString.substring(bAsString.length());
        } else if (aAsString.length() < bAsString.length()) {
            resultAsString += bAsString.substring(aAsString.length());
        } // if they're the same length no digits unzipped

        return Integer.parseInt(resultAsString);
        //
    }

    /**
     * Q3. Complete the method sumArray that returns the sum of values
     * in a given int array. For example,
     *     sumArray(new int[]{1, 2 ,3})
     * should return 6 as the sum.
     */
    public int sumArray(int[] values) {
        //Answer here
        int result = 0;
        for (int i = 0; i < values.length; i++) {
            result += values[i];
        }
        return result;
        //
    }

    /**
     * Q4. Complete the method getBiggestValue that returns the maximum
     * value from a given int array. For example,
     *     getBiggestValue(new int[]{0, 12 ,101})
     * should return 101 as the biggest value.
     */
    public int getBiggestValue(int[] values) {
        //Answer here
        if (values.length == 0) { return 0; } //guard clause required as value[0] called outside for loop

        int biggest = values[0];    //hypothesis; first char is the biggest
        for (int i = 1; i < values.length; i++) {
            if (values[i] > biggest) {
                biggest = values[i];
            }
        }
        return biggest;
        //
    }

    /**
     * Q5. Complete the method countOnes that returns the number of values
     * that are equal to one from a given int array. For example,
     *     countOnes(new int[]{0, 1 ,1})
     * should return 2 as the number of ones from the given array.
     */
    public int countOnes(int[] values) {
        //Answer here
        int oneCount = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] == 1) {
                oneCount++;
            }
        }
        return oneCount;
        //
    }

    /**
     * Q6. Complete the method findMostFrequentInteger that returns the
     * most frequently occurring number in an integer array. For example,
     * given an integer array:
     *     {1, 2, 3, 4, 5, 1}
     * the method will return 1 as the most frequently occurring number.
     * If there are more than one most frequently occurring number, then
     * return the smallest number from the most frequently occurring
     * numbers. For example, given an integer array
     *     {2, 3, 3, 2, 4, 5, 4}
     * the method should return 2 as the most frequently occurring number.
     */
    public int findMostFrequentInteger(int[] values) {
        //Answer here
        if (values.length == 0) { return 0; } //guard clause required as value[0] called outside for loop

        int mostFrequent = values[0] + 1; //initialisation required so loop can run; must be different from first value otherwise loop continues incorrectly
        int occurrences = 0;

        for (int i = 0; i < values.length; i++) {

            if (values[i] == mostFrequent) { continue; }  //don't need to test it again
            int hits = 0;

            for (int j = 0; j < values.length; j++) {

                if (values[i] != values[j]) { continue; }
                hits++;

                if  (hits > occurrences) {
                    mostFrequent = values[i];
                    occurrences = hits;
                } else if ((hits == occurrences) && (values[i] < mostFrequent)) {
                    mostFrequent = values[i];
                    //occurrences don't need to be changed
                }
            }
        }

        return mostFrequent;
        //
    }

    /**
     * Q7. Complete the method lastIndexOf that returns the
     * index position of last occurrence of a given value in an integer array.
     * For example, given an integer array
     *      {1, 2, 3, 1, 4}
     *      and an integer value 1
     * the method will return 3 as the last index position of the given value.
     * If the array does not contain the given value, then the method should
     * return -1.
     */
    public int lastIndexOf(int[] values, int value) {
        //Answer here
        for (int i = values.length; i > 0; i--) {   //offsetting the length instead of in the loop results in a 1 length array never being evaluated properly
            if (values[i - 1] == value) {
                return i - 1;
            }
        }
        return -1;
        //
    }

    /**
     * Q8. Complete the method range that returns the difference between
     * the maximum and minimum values in an integer array.
     * For example, given an integer array
     *      {1, 2, 3, 1, 4}
     * the method will return 3 as the range.
     * If the integer array only contains one value, then the method should
     * return the value itself.
     *
     * You may assume that there is always at least one value in the given
     * array.
     */
    public int range(int[] values) {
        //Answer here
        if (values.length == 1) { return values[0]; }

        int biggest = values[0];
        int smallest = values[0];

        for (int i = 1; i < values.length; i++) {

            if (values[i] > biggest) {
                biggest = values[i];

            } else if (values[i] < smallest) {
                smallest = values[i];
            }
        }

        return biggest - smallest;
        //
    }

    /**
     * Q9. Write the method computeFibonacci() that returns an integer
     * array of Fibonacci sequence, the size of which is controlled by a given
     * positive integer number. A Fibonacci sequence is a series of numbers,
     * where the next number is the sum of the previous numbers. For example,
     * if the method is given the number 6, it will return an integer array
     * with size 6 consisting the following numbers: 1, 1, 2, 3, 5, 8.
     */
    public int[] computeFibonacci(int size) {
        //Answer here
        if (size == 0) { return new int[]{0}; }
        /*https://www.classroom.co.nz/classrooms/3fed6511-b75f-492b-9f3a-82dcbfbc86fe/questions/9ae7e74b-e8cd-49a4-92d2-5199093d560f
            per answers here, returning an empty array is beyond the scope of the question
        */


        int[] result = new int[size];

        result[0] = 1;  //always true as we are assured of a positive integer, and the 0 case has been handled above
        if (size >= 2) { result[1] = 1; } //could be an else if but that would duplicate assignment of 0th index

        for (int i = 2; i < result.length; i++) {
            result[i] = result[i - 1] + result[i -2];
        }
        return result;
        //Answer here
    }

    /**
     * Q10. Complete the method findUniqueNumber that returns the unique
     * number which is not repeated in the given array
     * For example, given an integer array
     *      {1, 2, 3, 2, 3}
     * the method will return 1 as unique number.
     * If the integer array only contains one value, then the method should
     * return the value itself. If the integer array only contains the same
     * duplicated number, then the method should return that value. If there
     * are more than one unique number in the given array, then the method
     * should return the smallest value from the unique numbers.
     *
     * You may assume that there is always at least one value in the given
     * array.
     */
    public int findUniqueNumber(int[] values) {
        //Answer here
        if (values.length == 1) { return values[0]; }   //return early for this case that is easily proven and isn't correctly proven by the method below

        int falseHypothesis = getBiggestValue(values) + 1;
        int mostUnique = falseHypothesis;
        /*  This method can't reliably disprove that some n is the most unique number u, when u > n.
            In that case it will instead incorrectly return n because of the tie breaker logic.
            Rather than designing the method to reliably test the hypothesis, the 'hypothesis' is the largest
            value in the array + 1, ensuring that that number will be disproven by any element in the array,
            If it is never disproven we can be sure that there was no unique number in the array (or the array is all one number).
         */

        int firstValue = values[0];
        boolean isAllOneNumber = true;  //assume it is all one number

        for (int i = 0; i < values.length; i++) {
            boolean isUnique = true;    //assume it is unique

            for (int j = 0; j < values.length; j++) {

                if (i == j) {continue;} //don't compare an element with itself

                if (values[i] == values[j]) {
                    isUnique = false; //our uniqueness hypothesis is wrong, we can stop testing this number
                    break;
                }
            }

            if (isUnique && values[i] < mostUnique) {   //save number if it's 'more' unique (per specs) than the current one
                mostUnique = values[i];
            }

            if (isAllOneNumber) { isAllOneNumber = firstValue == values[i]; } //if our all one number hypothesis is still true, check if it's true for this number

        }

        if (mostUnique == falseHypothesis && !isAllOneNumber) { //no unique number found & not just all one number
            mostUnique = 0;    //no unique number found; the specs don't say what to do in this case, so I've decided to return 0 as it's falsy
        } else if (isAllOneNumber) {
            mostUnique -= 1;    //the false hypothesis has never been disproven, so subtracting one returns the only number in the array
        }

        return mostUnique;
        //
    }


}
